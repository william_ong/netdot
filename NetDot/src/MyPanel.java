// William Ong
// 06/05/2023
// The code in this file creates a MyPanel class, which is a child class of the JPanel class and contains additional attributes and methods to implement a GUI, perform logic for the Dots game, and update the game display.

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import javax.swing.JPanel;
import java.util.LinkedList;

// the MyPanel class is a child class of the JPanel class and contains additional attributes and methods that help implement the GUI, perform logic for the Dots game, and update the game display
// methods include initializing the grid of boxes, saving the NetDot frame, setting the display to a running state (disabling elements and saving player information), saving mouse clicks, calculating which edge should be drawn,
// setting such edges to a drawn state, marking the owners of completed boxes, displaying the grid and lines, updating the information labels, keeping track of the score, etc.
public class MyPanel extends JPanel {
	LinkedList<Point> points = new LinkedList<Point>(); // constructs and assigns a LinkedList of the Point class to instance var "points"
	private Graphics g; // stores the Graphics object of the panel
	private NetDot parentFrame; // stores a NetDot frame
	private Box[][] boxGrid = new Box[8][8]; // constructs and assigns an 8x8 2D array of the Box class to instance var "boxGrid"
	private int boxLen = 50; // number of pixels per box edge
	private String player1Name; // stores player 1's name (server/host)
	private String player2Name; // stores player 2's name (client)
	private char player1Initial; // stores player 1's initial
	private char player2Initial; // stores player 2's initial
	public boolean player1Turn = true; // stores the player's turn - true if player 1's turn and false if player 2's turn
	public boolean gameEnded = false; // stores whether the game has ended 
	private boolean errorMessage = false; // stores whether an error message should be displayed
	private int player1Score = 0; // stores player 1's score
	private int player2Score = 0; // stores player 2's score

	// initGrid() initializes the 8x8 2D array of the Box class
	public void initGrid() {
		for (int row = 0; row < 8; row++) { // code below loops through each row from 0 to 7
			for (int col = 0; col < 8; col++) { // code below loops through each column from 0 to 7
				boxGrid[row][col] = new Box(); // constructs and assigns a Box to var boxGrid[row][col]
			}
		}
	}
	
	// saveFrame() saves the NetDot object in the method's argument to the instance var "parentFrame"
	// arguments include a NetDot object
	public void saveFrame(NetDot frame) {
		parentFrame = frame; // saves arg object to instance var "parentFrame"
	}
	
	// savePoint() adds the Point in the method's argument to the instance var LinkedList "points" - stores the locations of mouse clicks
	// arguments include a Point object
	public void savePoint(Point p) {
		points.add(p); // adds Point arg to LinkedList "points"
	}
	
	// paint() paints the panel, displays the grid of boxes, and calls various methods to update the grid, display the selected lines and owners, update the information labels, and check if the game has ended
	// arguments include a Graphics object, which stores the Graphics object of the panel
	public void paint(Graphics g) {
		super.paint(g); // calls the paint method of the parent class (original paint method) - arguments include the panel's Graphics object
		this.g = g; // assigns Graphics arg to instance var "g"
		// code below displays the grid of boxes by drawing small rectangles every increment of "boxLen" horizontally and vertically, creating a 9x9 grid of dots
		for (int row = boxLen; row < boxLen * 10; row += boxLen) {
			for (int col = boxLen; col < boxLen * 10; col += boxLen) {
				g.setColor(Color.GRAY);
				g.fillRect(row, col,  2,  2);
			}
		}
		if (parentFrame.connectionEstablished) { // code below is executed if the connection has been established between server and client
			// calls various methods to update the grid, display the selected lines and owners, update the information labels, and check if the game has ended
			calculateBoxEdge();
			markCompletedBoxes();
			paintLines();
			updateInfoLabels();
			checkGameEnd();
		} 
	}
	
	// calculateBoxEdge() determines the closest edge to a mouse click and sets that edge to a drawn status
	private void calculateBoxEdge() {
		if (points.size() != 0) { // code below is executed if LinkedList "points" isn't empty
			Point p = points.get(0); // assigns the first node of "points" to var "p"
			int row = (int)((p.getY() - boxLen)/boxLen); // calculates the row the point is in
			int col = (int)((p.getX() - boxLen)/boxLen); // calculates the column the point is in
			if (row < 8 && col < 8 && p.getY() > boxLen && p.getX() > boxLen) { // code below is executed if the point is in the grid
				int dU = (int)p.getY() - boxLen - (row * boxLen); // distance from point to top edge
				int dB = ((row + 1) * boxLen) - (int)p.getY() + boxLen; // distance from point to bottom edge
				int dL = (int)p.getX() - boxLen - (col * boxLen); // distance from point to left edge
				int dR = ((col + 1) * boxLen) - (int)p.getX() + boxLen; // distance from point to right edge
				// code below determines which edge the point is closest to and calls setBoxEdge() to set the respective edge to a drawn status
				// arguments include the calculated row, calculated column, and a char that stores the desired edge
				// 't' = top	'b' = bottom	'l' = left		'r' = right
				if (dU < dB && dU < dL && dU < dR) {
					setBoxEdge(row, col, 't');
				} else if (dB < dU && dB < dL && dB < dR) {
					setBoxEdge(row, col, 'b');
				} else if (dL < dU && dL < dB && dL < dR) {
					setBoxEdge(row, col, 'l');
				} else {
					setBoxEdge(row, col, 'r');
				}
			} else { // code below is executed if the point is not in the grid
				changeTurn(); // calls changeTurn(), which keeps the player's turn, as changeTurn() is already called in the NetDot class (double negation)
			}
			points.remove(0); // removes the first node of "points" (every point is removed after processing)
		}
	}
	
	// setBoxEdge() checks if a box edge is already drawn - if not, setBoxEdge() sets the desired edge to a drawn status - otherwise, setBoxEdge() signals for an error message to be displayed and lets the player keep their turn
	// arguments include the row, column, and edge of a Box to be manipulated
	private void setBoxEdge(int row, int col, char edge) {
		if (boxGrid[row][col].getBoxEdge(edge)) { // code below is executed if the edge is already drawn
			changeTurn(); // calls changeTurn(), which keeps the player's turn, as changeTurn() is already called in the NetDot class (double negation)
			errorMessage = true; // prompts an error message to be displayed
		} else { // code below is executed if the edge hasn't been drawn
			// code below sets the desired edge to a drawn status as well as the edge of adjacent boxes if applicable
			// 't' = top	'b' = bottom	'l' = left		'r' = right
			if (edge == 't') {
				boxGrid[row][col].setBoxEdge('t');
				if (row != 0) {
					boxGrid[row-1][col].setBoxEdge('b'); // sets the bottom edge of upper-adjacent box to a drawn status
				}
			} else if (edge == 'b') {
				boxGrid[row][col].setBoxEdge('b');
				if (row != 7) { 
					boxGrid[row+1][col].setBoxEdge('t'); // sets the top edge of lower-adjacent box to a drawn status
				}
			} else if (edge == 'l') {
				boxGrid[row][col].setBoxEdge('l');
				if (col != 0) {
					boxGrid[row][col-1].setBoxEdge('r'); // sets the right edge of left-adjacent box to a drawn status
				}
			} else {
				boxGrid[row][col].setBoxEdge('r');
				if (col != 7) {
					boxGrid[row][col+1].setBoxEdge('l'); // sets the left edge of right-adjacent box to a drawn status
				}
			}
			errorMessage = false; // error message should not be displayed (valid line selection)
		}
	}
	
	// markCompletedBoxes() marks the owner of completed boxes, keeps track of each player's score, and checks if a player should keep their turn
	private void markCompletedBoxes() {
		int prevTotalBoxesCompleted = player1Score + player2Score; // prevTotalBoxesCompleted is assigned the initial number of boxes completed (before marking new boxes that are completed)
		for (int row = 0; row < 8; row++) { // code below loops through each row from 0 to 7
			for (int col = 0; col < 8; col++) { // code below loops through each column from 0 to 7
				if (boxGrid[row][col].checkBoxComplete() && boxGrid[row][col].getOwner() == '_') { // code below is executed if boxGrid[row][col] is completed and has not been claimed
					if (player1Turn) { // code below is executed if it is now player 1's turn
						boxGrid[row][col].setOwner(player2Initial); // assigns player 2's initial to the owner of the completed box
						player2Score++; // increments player 2's score by 1
					} else { // code below is executed if it is now player 2's turn
						boxGrid[row][col].setOwner(player1Initial); // assigns player 1's initial to the owner of the completed box
						player1Score++; // increments player 1's score by 1
					}
				}
			}
		}
		if (prevTotalBoxesCompleted != (player1Score + player2Score)) { // code below is executed if a player completed a box with their selected line, indicating they should keep their turn
			changeTurn(); // calls changeTurn(), which keeps the player's turn, as changeTurn() is already called in the NetDot class (double negation)
		}
	} 

	// paintLines() displays the completed lines and the owners of completed boxes
	private void paintLines() {
		for (int row = 0; row < 8; row++) { // code below loops through each row from 0 to 7
			for (int col = 0; col < 8; col++) { // code below loops through each column from 0 to 7
				// code below checks if each edge of boxGrid[row][col] should be drawn - if so, the respective line is drawn by calling g.drawLine()
				if (boxGrid[row][col].getBoxEdge('t')) {
					g.drawLine((col * boxLen) + boxLen, (row * boxLen) + boxLen, (col * boxLen) + (boxLen * 2), (row * boxLen) + boxLen); // draws top edge
				} 
				if (boxGrid[row][col].getBoxEdge('b')) {
					g.drawLine((col * boxLen) + boxLen, (row * boxLen) + (boxLen * 2), (col * boxLen) + (boxLen * 2), (row * boxLen) + (boxLen * 2)); // draws bottom edge 
				}
				if (boxGrid[row][col].getBoxEdge('l')) {
					g.drawLine((col * boxLen) + boxLen, (row * boxLen) + boxLen, (col * boxLen) + boxLen, (row * boxLen) + (boxLen * 2)); // draws left edge
				} 
				if (boxGrid[row][col].getBoxEdge('r')) {
					g.drawLine((col * boxLen) + (boxLen * 2), (row * boxLen) + boxLen, (col * boxLen) + (boxLen * 2), (row * boxLen) + (boxLen * 2)); // draws right edge 
				}
				if (boxGrid[row][col].getOwner() != '_' ) { // code below is executed if a box has an owner (completed box)
					// displays the owner's initial in the center of the box
					g.drawString(Character.toString(boxGrid[row][col].getOwner()), (boxLen * col) + (boxLen / 2) + boxLen, (boxLen * row) + (boxLen / 2) + boxLen);
				}
			}
		}
	}
	
	// checkGameEnd() checks if the game has ended by checking if all boxes have been completed - if so, checkGameEnd() displays the winner (or tie) and indicates that the game has ended
	private void checkGameEnd() {
		if (player1Score + player2Score == 64) { // code below is executed if all boxes are completed (game finished)
			if (player1Score > player2Score) { // code below is executed if player 1 has more completed boxes than player 2
				parentFrame.infoLabel.setText(player1Name + " wins!"); // displays that player 1 has won
			} else if (player1Score < player2Score) { // code below is executed if player 2 has more completed boxes than player 1
				parentFrame.infoLabel.setText(player2Name + " wins!"); // displays that player 2 has won
			} else { // code below is executed if players 1 and 2 have the same number of completed boxes
				parentFrame.infoLabel.setText("Game ends in a tie!"); // notifies players of the tie
			}
			gameEnded = true; // indicates that game has ended
		}
	}
	
	// updateInfoLabels() updates the labels that display the score, potential errors, and the player's turn
	private void updateInfoLabels() {
		if (errorMessage) { // code below is executed if an error message should be displayed
			parentFrame.scoreLabel.setText("That edge is already taken."); // notifies user of error
		// code below is executed if there aren't any errors to be displayed
		} else {
			parentFrame.scoreLabel.setText("Score:    " + printPlayerScores());
			if (player1Turn) { // code below is executed if it is player 1's turn
				parentFrame.infoLabel.setText("Turn:     " + player1Name);
			} else { // code below is executed if it is player 2's turn
				parentFrame.infoLabel.setText("Turn:     " + player2Name);
			}
		}
	}
	
	// printPlayerScores() returns a String that stores the scores in the form of "player1Initial: player1Score		player2Initial: player2Score"
	private String printPlayerScores() {
		return (player1Initial + ": " + player1Score + "  " + player2Initial + ": " + player2Score);
	}
		
	// changeTurn() changes the player's turn
	public void changeTurn() {
		player1Turn = !(player1Turn); // assigns negated player1Turn to player1Turn (false becomes true and true becomes false)
	}
	
	// runningConfig() sets the game display to a running state by disabling various GUI elements and storing player information
	// arguments include two strings, which should store the player names
	public void runningConfig(String player1Name, String player2Name) {
		// code below assigns the method args to respective player name instance vars
		this.player1Name = player1Name;
		this.player2Name = player2Name;
		// code below assigns the first char of each name to the respective instance var player initial
		player1Initial = player1Name.charAt(0);
		player2Initial = player2Name.charAt(0);
		// code below displays both player names in the name input text fields
		parentFrame.player1InputName.setText(player1Name);
		parentFrame.player2InputName.setText(player2Name);
		// code below disables the radio buttons, the ability to input a name, and the host address input field
		parentFrame.serverButton.setEnabled(false);
		parentFrame.clientButton.setEnabled(false);
		parentFrame.player1InputName.setEnabled(false);
		parentFrame.player2InputName.setEnabled(false);
		parentFrame.hostAddress.setEnabled(false);
		parentFrame.connectionEstablished = true; // notifies parentFrame that a connection has been established
		repaint(); // requests a call for paint() - updates the game display
	}
}