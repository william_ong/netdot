// William Ong
// 06/05/2023
// The code in this file creates a NetDot class, which is a child class of the JFrame class and contains additional attributes to implement a GUI for the Dots game that can be played over a network.
// The Dots game is played with 2 people and contains a 2-dimensional grid of 9x9 dots, which represents an 8x8 grid of boxes. To play the game, players must first select whether they want to be the server or the client.
// Users can host a game by selecting the server radio button and clicking "START" or join a game by selecting the client radio button, entering the host ID, and clicking "CONNECT".
// Users can also enter their names. However, by default, the respective names are "Server" and "Client". Once a connection is established, players can take turns clicking between dots, drawing a line between 
// the dots. Lines can be drawn horizontally or vertically, and whenever a box is completed, the player who completed the box will have another turn, and their first-initial will appear in the center of the box. 
// After all boxes have been completed, the game will announce the winner, which is determined by which player has the most boxes completed. To end the game, players can click "QUIT".

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JRadioButton;

// the NetDot class is a child class of the JFrame class and contains additional attributes to implement a GUI for the Dots game that can be played over a network
// the class contains a main() method, a constructor for a NetDot frame, threads that establish a network connection,
// and various event handlers that process user input and calls methods required for the implementation of the game
public class NetDot extends JFrame {
	private MyPanel contentPane; // instance variable of the "MyPanel" class
	// the following instance variables are public because they are used in other classes 
	public JTextField player1InputName; // text fields that allow users to input their names
	public JTextField player2InputName;
	public JRadioButton serverButton; // radio buttons that allow users to select to host or join a game
	public JRadioButton clientButton;
	public JLabel infoLabel; // label that displays player turn and potential errors
	public JLabel scoreLabel; // label that displays the score
	public JButton menuButton; // button to control the game (Start, Connect, or Quit)
	public JTextField hostAddress; // text field that allows client to enter a host address
	public boolean connectionEstablished = false; // stores whether a connection has been established between server and client - true if established and false otherwise
	
	// main() detects when the program is ran and tries to construct and display a NetDot frame
	// arguments include an array of strings
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			// run() tries to construct and display a NetDot frame when the program is ran
			public void run() {
				try { // tests code below for errors
					NetDot frame = new NetDot(); // constructs and assigns a NetDot frame to var "frame"
					frame.setVisible(true); // displays the NetDot frame
				} catch (Exception e) { // code below is executed if the code in the try block threw exceptions
					e.printStackTrace();
				}
			}
		});
	}

	// NetDot() creates the frame for the Dots game's GUI, contains various event handlers that process user input, 
	// and creates threads that establishes the network connection, calling various methods required for the implementation of the game
	public NetDot() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 525); // sets the location and size of the frame
		contentPane = new MyPanel();  // constructs and assigns a MyPanel to instance var "contentPane"
		contentPane.setBorder(new EmptyBorder(50, 50, 50, 50));
		setContentPane(contentPane);
		contentPane.setLayout(null); // enables absolute positioning of graphical elements in "contentPane"
		contentPane.saveFrame(this); // calls saveFrame() with the current NetDot object as an argument
		
		infoLabel = new JLabel("Turn: Player 1"); // constructs and assigns a JLabel to instance var "infoLabel" - displays the initial state of the game (Player 1's turn and no errors)
		infoLabel.setFont(new Font("Segoe UI", Font.BOLD, 13)); // sets the label's font
		infoLabel.setBounds(45, 0, 400, 25); // sets the location and size of the label
		contentPane.add(infoLabel); // displays label
		
		scoreLabel = new JLabel("Score:"); // constructs and assigns a JLabel to instance var "scoreLabel" - displays the initial score of the game (no score)
		scoreLabel.setFont(new Font("Segoe UI", Font.BOLD, 13));
		scoreLabel.setBounds(45, 20, 400, 25);
		contentPane.add(scoreLabel); // displays label
		
		serverButton = new JRadioButton("Server"); // constructs and assigns a JRadioButton to instance var "serverButton", which allows players to select the option to host a game
		serverButton.setFocusable(false);
		serverButton.setFont(new Font("Dialog", Font.BOLD, 13));
		serverButton.setBounds(495, 45, 141, 23);
		contentPane.add(serverButton); // displays button
		
		JLabel player1NameLabel = new JLabel("Player 1"); // constructs and assigns a JLabel to var "player1NameLabel", which prompts for host's name
		player1NameLabel.setFont(new Font("Dialog", Font.BOLD, 13));
		player1NameLabel.setBounds(505, 80, 117, 14);
		contentPane.add(player1NameLabel); // displays label 
		
		player1InputName = new JTextField(); // constructs and assign a JTextField to instance var "player1InputName", which allows host to input their name
		player1InputName.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		player1InputName.setBounds(500, 106, 100, 20);
		contentPane.add(player1InputName); // displays text field
		player1InputName.setColumns(10);
		
		menuButton = new JButton("CONNECT"); // constructs and assigns a JButton to instance var "menuButton", which allows players to control the game (Start, Connect, or Quit)
		menuButton.setFont(new Font("Dialog", Font.BOLD, 12));
		menuButton.setBounds(500, 155, 100, 40);
		contentPane.add(menuButton); // displays button
		
		clientButton = new JRadioButton("Client"); // constructs and assigns a JRadioButton to instance var "clientButton", which allows players to select the option to join a game
		clientButton.setFocusable(false);
		clientButton.setFont(new Font("Dialog", Font.BOLD, 13));
		clientButton.setBounds(495, 220, 141, 23);
		contentPane.add(clientButton); // displays button
		
		JLabel player2NameLabel = new JLabel("Player 2"); // constructs and assigns a JLabel to var "player2NameLabel", which prompts for client's name
		player2NameLabel.setFont(new Font("Dialog", Font.BOLD, 13));
		player2NameLabel.setBounds(505, 255, 117, 14);
		contentPane.add(player2NameLabel); // displays label
		
		player2InputName = new JTextField(); // constructs and assign a JTextField to instance var "player2InputName", which allows client to input their name
		player2InputName.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		player2InputName.setBounds(500, 280, 100, 20);
		contentPane.add(player2InputName); // displays text field
		player2InputName.setColumns(10);
		
		hostAddress = new JTextField("localhost"); // constructs and assigns a JTextField to instance var "hostAddress", which allows players to input the host ID (default is localhost)
		hostAddress.setBounds(500, 310, 103, 26);
		player2InputName.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		contentPane.add(hostAddress); // displays text field
		hostAddress.setColumns(10);
	
		clientButton.setSelected(true); // default selection is joining a game
		player1InputName.setEditable(false); // disables text field of host's name (non-editable and not enabled)
		player1InputName.setEnabled(false);
		
		serverButton.addActionListener(new ActionListener() { // detects action on the button to host a game
			// actionPerformed() is executed when the button is clicked - sets menu to host state
			// arguments include an ActionEvent object, which indicates that action on the button has occurred
			public void actionPerformed(ActionEvent e) { 
				serverButton.setSelected(true); // selects server button
				player1InputName.setEditable(true); // enables text field of host's name (editable and enabled)
				player1InputName.setEnabled(true);
				menuButton.setText("START"); // changes button to "START" (start a game)
				clientButton.setSelected(false); // deselects client button
				player2InputName.setEditable(false); // disables text field of client's name (non-editable and not enabled)
				player2InputName.setEnabled(false);
				hostAddress.setVisible(false); // disables host ID entry
			}
		});
		
		clientButton.addActionListener(new ActionListener() { // detects action on the button to join a game
			// actionPerformed() is executed when the button is clicked - sets menu to client state
			// arguments include an ActionEvent object, which indicates that action on the button has occurred
			public void actionPerformed(ActionEvent e) {
				clientButton.setSelected(true); // selects client button
				player2InputName.setEditable(true); // enables text field of client's name (editable and enabled)
				player2InputName.setEnabled(true);
				hostAddress.setVisible(true); // makes host ID entry visible
				menuButton.setText("CONNECT"); // changes button to "CONNECT" (connect to a game)
				serverButton.setSelected(false); // deselects server button
				player1InputName.setEditable(false); // disables text field of host's name (non-editable and not enabled)
				player1InputName.setEnabled(false);
			}
		});
		
		// constructs a NetThread and assigns to var "serverThread" - arguments include the current NetDot object, the MyPanel object, and an indication that the desired config is server
		NetThread serverThread = new NetThread(this, contentPane, true);
		// constructs a NetThread and assigns to var "clientThread" - arguments include the current NetDot object, the MyPanel object, and an indication that the desired config is client
		NetThread clientThread = new NetThread(this, contentPane, false);
		contentPane.initGrid(); // calls initGrid(), which initializes the default 8x8 grid of boxes
		menuButton.addMouseListener(new MouseAdapter() { // detects action on the menu button
			// mouseClicked() is executed when the button is clicked - first click creates the server or client thread (dependent on selected radio button)
			// arguments include an ActionEvent object, which indicates that action on the button has occurred
			@Override
			public void mouseClicked(MouseEvent e) {
				if (!menuButton.getText().equals("QUIT")) { // code below is executed if game is not running
					menuButton.setText("QUIT"); // changes button to "QUIT"
					if (serverButton.isSelected()) { // code below is executed if user wants to host a game
						if (player1InputName.getText().length() == 0) {
							player1InputName.setText("Server"); // sets host name text field to "Server" if no entry
						}
						serverThread.start(); // starts the thread for the server's side
						// code below disables the host's ability to join a game (host can't join themselves)
						clientButton.setEnabled(false);
						player2InputName.setEnabled(false);
						hostAddress.setEnabled(false);
					} else if (clientButton.isSelected()) { // code below is executed if user wants to join a game 
						if (player2InputName.getText().length() == 0) {
							player2InputName.setText("Client"); // sets client name text field to "Client" if no entry
						}
						clientThread.start(); // starts the thread for the client's side
					}
				} else { // code below is executed if game is already running (user wants to quit)
					if (connectionEstablished) { // code below is executed if connection has been established between server and client
						return; // allows the threads to exit the game themselves
					}
					System.exit(0); // exits the game if connection hasn't been established (host decides to quit before client joins)
				}
			}
		});
	}
}