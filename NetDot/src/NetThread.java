// William Ong
// 06/05/2023 
// The code in this file creates a NetThread class, which is a child class of Thread and contains additional code to implement a network for the Dots game. 
// NetThread establishes the network connection, sends and receives information, and calls various methods to update the game display.

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

// the NetThread class is a child class of Thread and contains additional code to implement a network for the Dots game
// if NetThread is configured as a server, NetThread waits for network connections on port 1234 
// if NetThread is configured as a client, NetThread connects to the desired host ID on port 1234 (default is localhost)
// once a connection is established between the server and client (two running threads), NetThread allows the server and client to move in lock-step with one another to exchange information of mouse clicks, 
// simultaneously calling various methods to update the game display
public class NetThread extends Thread {
	private NetDot parentFrame; // stores a NetDot frame 
	private String player1Name; // stores host's name
	private String player2Name; // stores client's name
	private MyPanel contentPane; // stores a MyPanel - panel of NetDot frame - allows reference to "contentPane" without using "parentFrame.contentPane"
	private boolean isServer; // stores whether the NetThread object will be configured as a server - true if server config and false if client config
	
	// NetThread() constructs a NetThread by storing the constructor args in the instance vars
	// arguments include the GUI's NetDot object, the GUI's MyPanel object, and the desired NetThread configuration
	public NetThread(NetDot frame, MyPanel contentPane, boolean isServer) {
		parentFrame = frame; // saves NetDot arg to instance var "parentFrame"
		this.contentPane = contentPane; // saves MyPanel arg to instance var "contentPane"
		this.isServer = isServer; // saves desired configuration to instance var "isServer"
	}
	
	// run() is ran whenever NetThread is started
	// if NetThread is configured as a server, run() creates a ServerSocket to accept connections on port 1234
	// if NetThread is configured as a client, run() attempts to connect to the given host ID on port 1234 (default is localhost)
	// in both configurations, NetThread creates a Scanner and PrintWriter to send and receive information
	// once the connection is established, NetThread waits for events, calling various methods to update the game display and/or sending mouse clicks to the other player
	public void run() {
		Scanner sc; // declares var "sc" as a Scanner
		PrintWriter pw; // declares var "pw" as a PrintWriter
		Socket sock; // declares var "sock" as a Socket
		if (isServer) { // code below is executed if desired thread configuration is server
			ServerSocket serverSock; // declares var "serverSock" as a ServerSocket
			try { // tries code below
				serverSock = new ServerSocket(1234); // creates a ServerSocket to accept connections on port 1234
			} catch (IOException e) { // code below is executed if ServerSocket construction threw an I/O exception
				e.printStackTrace();
				System.exit(0); // ends the program
				return;
			}
			try { // tries code below
				sock = serverSock.accept(); // waits for a connection to "serverSock" and accepts it - assigns connection to "sock"
			} catch (IOException e) { // code below is executed if establishing the connection threw an I/O exception
				e.printStackTrace();
				System.exit(0); // ends the program
				return;
			}
		} else {  // code below is executed if desired thread configuration is client
			try { // tries code below
				sock = new Socket(parentFrame.hostAddress.getText(), 1234); // creates a Socket that connects to the desired host ID on port 1234 (default is localhost) - assigns connection to "sock"
			} catch (UnknownHostException e) { // code below is executed if the IP address of the host cannot be determined
				System.out.println(e); // prints the exception
				System.exit(0); // ends the program
				return;
			} catch (IOException e) { // code below is executed if Socket construction threw an I/O exception
				System.out.println(e); // prints the exception
				System.exit(0); // ends the program
				return;
			}
		}
		
		try { // tries code below
			sc  = new Scanner(sock.getInputStream()); // constructs a Scanner - input stream is sock's input (receive information from connection)
		} catch (IOException e) { // code below is executed if Scanner construction threw an I/O exception
			System.out.println(e);
			System.exit(0); // ends the program
			return;
		}
		
		try { // tries code below
			pw = new PrintWriter(sock.getOutputStream()); // constructs a PrintWriter - output stream is sock's output (send information to connection)
		} catch (IOException e) { // code below is executed if PrintWriter construction threw an I/O exception
			e.printStackTrace();
			System.exit(0); // ends the program
			return;
		}
		
		if (isServer) { // code below is executed if desired thread configuration is server
			if (parentFrame.player1InputName.getText().length() == 0) { // code below is executed if host name is empty
				player1Name = "Server"; // default host name
			} else { // code below is executed if host name is empty (host cleared name while waiting for connection)
				player1Name = parentFrame.player1InputName.getText(); // assigns host's name to instance var "player1Name"
			}
			pw.println(player1Name); // prints host's name to output stream
			pw.flush(); // forces buffered output to be sent (sends info to client)
			player2Name = sc.nextLine(); // reads in the first line from client and assigns to "player2Name" (client's name)
		} else { // code below is executed if desired thread configuration is client
			player2Name = parentFrame.player2InputName.getText(); // assigns the input text field of player 2's name to instance var "player2Name"
			pw.println(player2Name); // prints client's name to output stream
			pw.flush(); // forces buffered output to be sent (sends info to server)
			player1Name = sc.nextLine(); // reads in the first line from server and assigns to "player1Name" (host's name)
		}
		contentPane.runningConfig(player1Name, player2Name); // calls contentPane's method runningConfig() - arguments include the player names
		contentPane.addMouseListener(new MouseAdapter() { // detects mouse-related events
			// mouseClicked() is executed when a mouse click is detected - checks for player turn - if it's their turn and they clicked, the game's display is updated - otherwise, nothing changes
			// arguments include a MouseEvent object, which indicates that a mouse-related event has occurred
			@Override
			public void mouseClicked(MouseEvent e) {
				if (!contentPane.gameEnded) { // code below is executed if game hasn't ended 
					if (isServer && contentPane.player1Turn || !isServer && !contentPane.player1Turn ) { // code below is executed if a player clicked while it's their turn
						contentPane.savePoint(new Point(e.getX(), e.getY())); // calls savePoint(), which saves the Point arg created with the coordinates of where the mouse click occurred
						pw.println("C" + "," + e.getX() + "," + e.getY()); // prints the coordinates of the mouse click in the form of ("C,x-cord,y-cord") to output stream
						pw.flush(); // forces buffered output to be sent (sends mouse click to other player)
						contentPane.repaint(); // requests a call for paint() - updates the game display
						contentPane.changeTurn(); // calls changeTurn(), which changes the player's turn
					}
				}
			}
		});
		
		parentFrame.menuButton.addMouseListener(new MouseAdapter() { // detects action on the menu button (player wants to quit)
			// actionPerformed() is executed when the button is clicked, which notifies the opponent that the game has ended and ends the game for the player who clicked quit
			// arguments include an ActionEvent object, which indicates that action on the button has occurred
			@Override
			public void mouseClicked(MouseEvent e) {
				pw.println("Q"); // prints a quit message to output stream
				pw.flush(); // forces buffered output to be sent (sends quit message to other player)
				System.exit(0); // ends the game for player who clicked quit
			}
		});
		
		while (sc.hasNextLine()) { // code below repeats until other player stops sending information (essentially a forever loop)
			String line = sc.nextLine(); // assigns a line of other player's input to var "line"
			String[] arr = line.split(","); // splits the input line around "," and assigns results to String array "arr"
			if (arr[0].equals("Q")) { // code below is executed if other player sent a quit message
				System.exit(0); // ends the game for the current player
			} else { // code below is executed if input isn't a quit message (mouse click)
				contentPane.savePoint(new Point(Integer.parseInt(arr[1]), Integer.parseInt(arr[2]))); // calls savePoint(), which saves the Point arg created with the coordinates of where the other player's mouse click occurred
			}
			contentPane.repaint(); // requests a call for paint() - updates the game display
			contentPane.changeTurn(); // calls changeTurn(), which changes the player's turn
		}
	}
}
