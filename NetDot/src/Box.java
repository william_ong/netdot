// William Ong
// 05/22/2023
// The code in this file creates a Box class, which stores information about the edges of a box and the owner, and includes various instance methods to view and manipulate the box's data.

// the Box class includes various instance variables to store information regarding a box's edges and owner, includes a Box constructor, and includes various instance methods to analyze and manipulate the box's data
public class Box {
	private boolean top; // instance variable to store the top edge's drawing status - true if drawn and false otherwise
	private boolean bottom; // bottom edge's drawing status
	private boolean left; // left edge's drawing status
	private boolean right; // right edge's drawing status
	private char owner; // box's owner
	
	// Box() constructs a Box by assigning the default values (non-drawn box) to the instance variables
	public Box() {
		top = false;
		bottom = false;
		left = false;
		right = false;
		owner = '_';
	}
	
	// checkBoxComplete() returns a boolean that stores whether all of the box edges have been drawn - true if all of the box edges are drawn and false otherwise
	public boolean checkBoxComplete() {
		return (top == true && bottom == true && left == true && right == true);
		// returns true if all of the box edges are drawn
	}
	
	// setBoxEdge() sets the respective edge of a box to a drawn status
	// arguments include a char, which should store the desired edge of the box ('t', 'b', 'l', or 'r')
	// 't' = top	'b' = bottom	'l' = left		'r' = right
	public void setBoxEdge(char edge) {
		// code below sets the desired edge of the box to a drawn status
		if (edge == 't') {
			top = true;
		} else if (edge == 'b') { 
			bottom = true; 
		} else if (edge == 'l') { 
			left = true; 
		} else if (edge == 'r') {
			right = true;
		}
	}
	
	// getBoxEdge() returns the status of the respective edge of a box - true if the edge is already drawn and false otherwise
	// arguments include a char, which should store the desired edge of the box ('t', 'b', 'l', or 'r')
	// 't' = top	'b' = bottom	'l' = left		'r' = right
	public boolean getBoxEdge(char edge) {
		// code below returns the status of the desired edge of the box
		if (edge == 't') {
			return top;
		} else if (edge == 'b') {
			return bottom;
		} else if (edge == 'l') {
			return left;
		} else if (edge == 'r') {
			return right;
		}
		return false; // returns false if the "edge" arg is an invalid char
	}
	
	// setOwner() sets the owner of the box to the char argument
	// arguments include a char, which should store the player's initial that completed the box
	public void setOwner(char initial) {
		owner = initial; // sets instance var "owner" to arg "initial"
	}
	
	// getOwner() returns a char, which stores the owner of the box
	public char getOwner() {
		return owner; // returns the owner of the box
	}
}